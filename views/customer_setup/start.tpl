<!--start.tpl start-->
% include('header.tpl', title='Page Title')

<div class="container">
    <div class="page-header">
        <h4>{{ str(env).upper() }} - New Customer Setup</h4>
    </div>
    <div class="row">
        <div class="col-sm-9">
            <a class="btn btn-primary" href="/{{ env }}/customer-setup" role="button">Back to List</a>
        </div>
        <div class="col-sm-9">
            <p></p>
        </div>
        <div class="col-lg-6">

            %if defined("db_id"):
            <div class="alert alert-danger">
                <p>Customer already exists with this name "{{ db_id }}"</p>
            </div>
            %end

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Search Customer ID</h3>
                </div>
                <div class="panel-body">
                    <div class="col-lg-6">
                        <form class="form-horizontal" role="form" action="search-name" method="post">
                            <div class="input-group">
                                <input type="text" name="name" class="form-control" pattern="[A-Za-z-0-9]+" title="CustomerId" required>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Go!</button>
                                </span>
                            </div><!-- /input-group -->
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

% include('footer.tpl')
<!--start.tpl end-->
