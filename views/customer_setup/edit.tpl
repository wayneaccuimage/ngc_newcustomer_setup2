<!--edit.tpl start-->
% include('header.tpl', title='Page Title')

<div class="container">
    <div class="row">
        <div class="page-header col-sm-11">
            <h4>{{ str(data.customer_info.env).upper() }} - Edit Customer Setup</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <a class="btn btn-primary" href="/{{ data.customer_info.env }}/customer-setup" role="button">Back to List</a>
        </div>
        <div class="col-sm-8">
        %if data.customer_info.env == "test":
            <a class="btn btn-primary pull-right" href="/production/customer-setup" role="button" target="_blank">Production</a>
        %end
        </div>

        % include('results.tpl', title='Page Title')


        <div class="col-sm-11">
            <form class="form-horizontal" role="form" method="post"
                  action="/{{ data.customer_info.env }}/customer-setup/{{ data.validation_update.id }}/edit">

                % include('customer_setup/add_edit.tpl')

                <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-4">
                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

% include('footer.tpl')
<!--edit.tpl end-->
