<!--new.tpl start-->
% include('header.tpl', title='Page Title')

<div class="container">
    %if data.customer_info.env == "test":
        <div class="row">
            <div class="col-sm-12">
                <p></p>
            </div>
            <!--<div class="col-sm-12">
                <a class="btn btn-primary pull-right" href="/production/customer-setup" role="button" target="_blank">Production</a>
            </div>-->
        </div>
    %end
    <div class="page-header">
        <h4>{{ str(data.customer_info.env).upper() }} - New Customer Setup</h4>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <a class="btn btn-primary" href="/{{ data.customer_info.env }}/customer-setup" role="button">Back to List</a>
        </div>
        <div class="col-sm-8">
        %if data.customer_info.env == "test":
            <a class="btn btn-primary pull-right" href="/production/customer-setup" role="button" target="_blank">Production</a>
        %end
        </div>
        <div class="col-sm-8">
            %if defined("show_clone") and show_clone:
                <a class="btn btn-primary pull-right"
                   href="/{{ data.customer_info.env }}/customer-setup/new?name={{ data.customer_info.name }}&cloneTest=yes"
                   role="button">Clone Test</a>
            %end
        </div>

        % include('results.tpl', title='Page Title')

        <div class="col-sm-11">
            <form class="form-horizontal" role="form" method="post" action="new">

                % include('customer_setup/add_edit.tpl')

                <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-4">
                        <button type="submit" class="btn btn-primary btn-block">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

% include('footer.tpl')
<!--new.tpl end-->
