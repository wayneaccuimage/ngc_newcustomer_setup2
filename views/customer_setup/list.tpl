<!--list.tpl start-->
% include('header.tpl', title='NGC Setup 2')

<div class="container">
    %if env == "test":
        <!--<div class="row">
            <div class="col-sm-12">
                <p></p>
            </div>
            <div class="col-sm-12">
                <a class="btn btn-primary pull-right" href="/production/customer-setup" role="button" target="_blank">Production</a>
            </div>
        </div>-->
    %end
    <div class="page-header">
        <h4>{{ str(env).upper() }} - Customer List</h4>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-lg-6">
                        <a class="btn btn-primary" href="/{{ env }}/customer-setup/start" role="button">Add New</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <table class="table table-hoover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>DBID</th>
                    </tr>
                </thead>
                <tbody>
                    %for validate_update in list:
                    <tr>
                        <td>{{ validate_update.id }}</td>
                        <td><a href="/{{ env }}/customer-setup/{{ validate_update.id }}/edit">{{ validate_update.db_id }}</a></td>
                    </tr>
                    %end
                </tbody>
            </table>
        </div>
    </div>
</div>

% include('footer.tpl')
<!--list.tpl end-->