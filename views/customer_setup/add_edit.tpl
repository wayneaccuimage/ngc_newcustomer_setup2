<!--add_edit.tpl start-->
<!-- name -->
<div class="panel panel-default">
    <div class="panel-body">

        <div class="form-group">
            <label for="name" class="col-sm-3 control-label">Customer ID</label>
            <div class="col-sm-9">
                <input type="hidden" name="name" value="{{ data.customer_info.name if data.customer_info.name else '' }}">
                <input type="text" class="form-control" id="name" name="name" value="{{ data.customer_info.name if data.customer_info.name else '' }}" disabled>
            </div>
        </div>

        <!-- full name -->
        <div class="form-group">
            <label for="fullName" class="col-sm-3 control-label">Customer Name</label>
            <div class="col-sm-9">
                <input type="hidden" name="dbId" value="{{ data.validation_update.db_id if data.validation_update.db_id else '' }}">
                <input type="text" class="form-control" name="fullName" id="fullName" value="{{ data.customer_info.full_name if data.customer_info.full_name else '' }}">
            </div>
        </div>

        <!-- date -->
        <div class="form-group">
            <label for="date" class="col-sm-3 control-label">Date</label>
            <div class="col-sm-9">
                <input type="date" class="form-control" id="date" name="date" value="{{ data.customer_info.date if data.customer_info.date else '' }}" required>
            </div>
        </div>

    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">

        <!-- ftp info -->
        <div class="form-group">
            <label class="col-sm-3 control-label">FTP</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="ftpUserName" name="ftpUserName"
                       placeholder="User Name" value="{{ data.customer_info.ftp_user_name if data.customer_info.ftp_user_name else '' }}" required>
            </div>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="ftpPassword" name="ftpPassword"
                       placeholder="Password" value="{{ data.customer_info.ftp_pwd if data.customer_info.ftp_pwd else '' }}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="ftpIncomingDirectory" class="col-sm-3 control-label">FTP Incoming Directory</label>
            <div class="col-sm-9">

                SFTP: "z:\SSH\NGC_Users" - FTP: "D:\iFtpSvc\NGC\users\"<br>
                <!--<input type="hidden" name="ftpIncomingDirectory"
                       value="{{ data.customer_info.ftp_incoming_directory if data.customer_info.ftp_incoming_directory else '' }}">-->
                <!-- removed disabled to allow edit-->
                <input type="text" name="ftpIncomingDirectory" class="form-control" id="ftpIncomingDirectory"
                       value="{{ data.customer_info.ftp_incoming_directory if data.customer_info.ftp_incoming_directory else '' }}">
            </div>
        </div>

        <div class="form-group">
            <label for="exceptionDirectory" class="col-sm-3 control-label">FTP Outgoing Directory</label>
            <div class="col-sm-9">
                SFTP: "\\SSH\NGC_Users" - FTP: "D:\iFtpSvc\NGC\users\"<br>
                <!--<input type="hidden" name="ftpOutgoingDirectory"
                       value="{{ data.customer_info.ftp_outgoing_directory if data.customer_info.ftp_outgoing_directory else '' }}">-->
                <!-- removed disabled to allow edit-->

                <input type="text" name="ftpOutgoingDirectory" class="form-control" id="ftpOutgoingDirectory"
                        value="{{ data.customer_info.ftp_outgoing_directory if data.customer_info.ftp_outgoing_directory else '' }}">
                        <br>Note: Change DB dvwebindex_fileupload.jobs.PATH column to ->z:\ssh\NGC_Users\ [customerId] \incoming\
            </div>
        </div>

    </div>
</div>

<div class="panel panel-info">

<div class="panel-heading">
    <b><a id="hideShowLink1" href="javascript:toggle('hideShowDiv1','hideShowLink1');" >  Hide </a>  Data Fields</b>
</div>

<div id="hideShowDiv1" style="display: block;" class="panel-body">
<div>
    The following Data base tables will be effected: DB:dvofficeupdate Table:validationupdate, DB:dvwebindex_fileupload Tables: dvwebindex_settings & jobs & search_fieldinfo_[customerId] DB:dvstream Tables reports & reportsrows
    <hr>
</div>


<!--display block start-->
<!-- field list -->
<div class="form-group">
    <div class="col-sm-12">
        <div class="col-sm-1">
            <label for="fieldList" control-label>Field List</label>
        </div>
        <div class="col-sm-11">
            <button id="btnTextNum" type="button" class="btn btn-primary">Add Text & Number to Column Names (if needed)</button>&nbsp;&nbsp;(Add if not present: TRACKING_NUMBER, SHIP_DATE & CARD_NUMBER)
        </div>
        <div class="col-sm-12">
            <p></p>
        </div>

        <div class="col-sm-6">
            <strong>Table:</strong>Column Names<br>validationupdate<em> [ {{ data.customer_info.name if data.customer_info.name else '' }} ]</em>
            <textarea class="form-control" id="fieldList" rows="30"
                  name="fieldList" required>{{ data.validation_update.field_list if data.validation_update.field_list else '' }}</textarea>
        </div>
        <div class="col-sm-6">
            <strong>Table:</strong>Display Names for Columns<br>search_fieldinfo<em> [ {{ data.customer_info.name if data.customer_info.name else '' }} ]</em>
            <textarea class="form-control" id="displayNameList" rows="30"
                  name="displayNameList" required>{{ data.display_names if data.display_names else '' }}</textarea>
        </div>

    </div>
</div>

<!-- Post Update Query list -->
<div class="form-group">
    <label for="postUpdateQueryList" class="col-sm-3 control-label">Post Update Query</label>
    <div class="col-sm-9">
        <textarea class="form-control" id="postUpdateQueryList" rows="8" name="postUpdateQueryList"
                  required>{{ data.validation_update.post_update_query_list if data.validation_update.post_update_query_list else '' }}</textarea>
    </div>
</div>

<!-- new_table -->
<div class="form-group">
    <label for="newTable" class="col-sm-3 control-label">New Table</label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="newTable" name="newTable" value="{{ data.validation_update.new_table if data.validation_update.new_table else '' }}" required>
    </div>
</div>

<!-- task -->
<div class="form-group">
    <label for="task" class="col-sm-3 control-label">Task</label>
    <div class="col-sm-9">
        <em>Note: If not "FULL_NAME" field but "FIRST_NAME" and "LAST_NAME" fields then remove "SPLIT_FULL_NAME;"</em>
        <textarea class="form-control" id="task" rows="4" name="task" required>{{ data.validation_update.task if data.validation_update.task else '' }}</textarea>
    </div>
</div>

<!-- Remove Character -->
<div class="form-group">
    <label for="removeCharacter" class="col-sm-3 control-label">Remove Character</label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="removeCharacter" name="removeCharacter"
               value="{{ data.validation_update.remove_character if data.validation_update.remove_character else '' }}" required>
    </div>
</div>

<!-- Email Success to list -->
<div class="form-group">
    <label for="emailSuccessToList" class="col-sm-3 control-label">Email Success To List</label>
    <div class="col-sm-9">
        <textarea class="form-control" id="emailSuccessToList" rows="3"
                  name="emailSuccessToList" required>{{ data.validation_update.email_success_to_list if data.validation_update.email_success_to_list else '' }}</textarea>
    </div>
</div>

<!-- Email Error to list -->
<div class="form-group">
    <label for="emailErrorToList" class="col-sm-3 control-label">Email Error To List</label>
    <div class="col-sm-9">
        <textarea class="form-control" id="emailErrorToList" rows="3"
                  name="emailErrorToList" required>{{ data.validation_update.email_error_to_list if data.validation_update.email_error_to_list else '' }}</textarea>
    </div>
</div>

<!-- delimiter group -->
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="fileConvertType" class="col-sm-3 control-label">File Convertor Type</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="fileConvertType" name="fileConvertType"
                       value="{{ data.validation_update.file_convert_type if data.validation_update.file_convert_type else '' }}">
            </div>
        </div>

        <div class="form-group">
            <label for="fieldDelimiter" class="col-sm-3 control-label">Field Delimiter</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="fieldDelimiter" name="fieldDelimiter"
                       value="{{ data.validation_update.field_delimiter if data.validation_update.field_delimiter else '' }}">
            </div>
        </div>

        <div class="form-group">
            <label for="newLineDelimiter" class="col-sm-3 control-label">New Line Delimiter</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="newLineDelimiter" name="newLineDelimiter"
                       value="{{ data.validation_update.new_line_delimiter if data.validation_update.new_line_delimiter else '' }}">
            </div>
        </div>
    </div>
</div>

<!--div class="form-group">
    <label for="newDirectory" class="col-sm-3 control-label">New Directory</label>
    <div class="col-sm-9">
        <input type="hidden" name="newDirectoryHidden" value="{{ data.validation_update.new_directory if data.validation_update.new_directory else '' }}">
        <input type="text" class="form-control" id="newDirectory" name="newDirectory"
               value="{{ data.validation_update.new_directory if data.validation_update.new_directory else '' }}" disabled>
    </div>
</div-->

<div class="form-group">
    <label for="exceptionDirectory" class="col-sm-3 control-label">Exception Directory</label>
    <div class="col-sm-9">
        <input type="hidden" name="exceptionDirectoryHidden" value="{{ data.validation_update.exception_directory if data.validation_update.exception_directory else '' }}">
        <input type="text" class="form-control" id="exceptionDirectory" name="exceptionDirectory"
                value="{{ data.validation_update.exception_directory if data.validation_update.exception_directory else '' }}" disabled>
    </div>
</div>

<div class="form-group">
    <label for="backupDirectory" class="col-sm-3 control-label">Backup Directory</label>
    <div class="col-sm-9">
        <input type="hidden" name="backupDirectoryHidden" value="{{ data.validation_update.backup_directory if data.validation_update.backup_directory else '' }}">
        <input type="text" class="form-control" id="backupDirectory" name="backupDirectory"
                value="{{ data.validation_update.backup_directory if data.validation_update.backup_directory else '' }}" disabled>
    </div>
</div>
<!--add_edit.tpl end-->
</div><!--panel panel-info end-->
</div><!--panel-body end-->


