<!DOCTYPE html>
<html lang="en">
<!--header.tpl start-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Accu-Image : Customer Setup 2</title>

    <!-- Bootstrap -->
    <link href="/static/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script language="javascript">
        function toggle(showHideDiv, switchTextDiv) {
            var ele = document.getElementById(showHideDiv);
            var text = document.getElementById(switchTextDiv);
            if(ele.style.display == "block") {
                    ele.style.display = "none";
                text.innerHTML = "Show ";
            }
            else {
                ele.style.display = "block";
                text.innerHTML = "Hide ";
            }
        }
    </script>
  </head>
<body>
<div class="container">
    <div class="panel panel-info">
         <div class="panel-heading"><h4>NGC Setup 2 Ver 9/16/2014</h4></div>
      <div class="panel-body">
        <div class="row">
            <div class="col-sm-2">
                <!-- Button trigger modal -->
                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#helpModal">
                  Program Overview
                </button>
            </div>
            <!-- Button trigger Fields modal -->
            <div class="col-sm-2">
                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#fieldsModal">
                  Fields Reference
                </button>
            </div>
            <div class="col-sm-2">
              <a href="http://ssh.accu-image.com/WSFTPSVR/" target="_blank">SFTP Server</a>
            </div>
        </div>
      </div>
    </div>
</div>

<!-- Modal Help -->
<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="helpModalLabel">NGC Customer Setup Overview</h4>
      </div>
      <div class="modal-body">
   <B>The Process</B>
<hr>
<B>1) Get form data</B><br>
	-> Validate the form data<br>
		Does customerId already exist?<br>
		Is all required data available in the form?<br>
2) <B>Submit</B><br>
3) <B>Backup current & create new updated- ValidationUpdate</B><br>
	Table: validationupdate<br>
4) <B>Backup current & create new updated - DVWebIndexFileUpload</B><br>
	- Table: DVWEBINDEX_SETTINGS<br>
	- If Add <br>
          Create new Table: SEARCHFIELD_INFO_[customerId]<br>
          else update Table: SEARCHFIELD_INFO_[customerId]<br>
	- Table: Jobs<br>
5) <B>Backup current & create new - DVStream</B><br>
	- If new resort tables<br>
		Table: reports<br>
		Table: reportsrows<br>
6) <B>Create FTP directories using customerId</B><br>
7) <B>Create or Update CustomerId Log file</B><br>
8) <B>Display Results</B>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal Fields Reference end -->
<!-- Modal -->
<div class="modal fade" id="fieldsModal" tabindex="-1" role="dialog" aria-labelledby="fieldsModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="fieldsModalLabel">Fields Reference List</h4>
      </div>
      <div class="modal-body">
            <!--<div class="col-sm-12">-->
            <strong>Table:</strong>Reference Column Name Mapping - <em>For Copy and Past</em>
            <textarea class="form-control" id="referenceList" rows="50" name="fieldList" required>
-- DEFAULT FIELDS LIST --

ORDER_ID,TEXT,NO;
BUSINESS_NAME,TEXT,NO;
FULL_NAME,TEXT,NO;
EMAIL,TEXT,NO;
PHONE,TEXT,NO;
ADDRESS_1,TEXT,NO;
ADDRESS_2,TEXT,NO;
CITY,TEXT,NO;
STATE,TEXT,NO;
ZIP,TEXT,NO;
COUNTRY,TEXT,NO;
ITEM,TEXT,NO;
CARD_NAME,TEXT,NO;
SHIPPING,TEXT,NO;
CARD_NUMBER,TEXT,SKIP;
REDEMPTION_ID,TEXT,NO;
MESSAGE_ENGLISH,TEXT,NO;
SHIP_DATE,TEXT,NO;
BATCH_QC_COMPLETED,TEXT,SKIP;
TRACKING_NUMBER,TEXT,SKIP;
LOB_ID,TEXT,NO;
CARD_COUNT_ID,TEXT,SKIP;

Order ID
Company Name
Recipient Name
Reipient Email Address
Recipient Phone Number
Ship to Address Line 1
Ship to Address line 2
Ship to City
Ship to State/Province
Ship To Postal Code
Ship to Country
Item SKU
Item Description
Shipment Type
Card Number
Pin
Message
Date Shipped
Status
Tracking
LOB Code
Card Count ID

-- VIEW --
staging`.`REFERENCE_ID` AS `REFERENCE_ID`,
staging`.`ORDER_ID` AS `ORDER_ID`,
staging`.`LINE_NUM` AS `LINE_NUM`,
staging`.`LOB_ID` AS `STAGING_LOB_ID`,
staging`.`LOB_ORDER_ID` AS `STAGING_LOB_ORDER_ID`,
staging`.`ORDER_DATE` AS `ORDER_DATE`,
staging`.`FULL_NAME` AS `FULL_NAME`,
staging`.`FIRST_NAME` AS `FIRST_NAME`,
staging`.`LAST_NAME` AS `LAST_NAME`,
staging`.`BUSINESS_NAME` AS `BUSINESS_NAME`,
staging`.`ADDRESS_1` AS `ADDRESS_1`,
staging`.`ADDRESS_2` AS `ADDRESS_2`,
staging`.`CITY` AS `CITY`,
staging`.`STATE` AS `STATE`,
staging`.`ZIP` AS `ZIP`,
staging`.`PHONE` AS `PHONE`,
staging`.`EMAIL` AS `EMAIL`,
staging`.`ITEM` AS `ITEM`,
staging`.`CARD_NAME` AS `CARD_NAME`,
staging`.`QTY` AS `QTY`,
staging`.`SHIPPING` AS `SHIPPING`,
staging`.`TRACKING_NUMBER` AS `STAGING_TRACKING_NUMBER`,
staging`.`VISA_CARD_EMBOSSED_LINE_4_INFO` AS `VISA_CARD_EMBOSSED_LINE_4_INFO`,
staging`.`SHIP_DATE` AS `STAGING_SHIP_DATE`,
staging`.`DENOMINATION` AS `DENOMINATION`,
staging`.`SEI_OID` AS `SEI_OID`,
staging`.`REDEMPTION_ID` AS `REDEMPTION_ID`,
staging`.`COUNTRY` AS `COUNTRY`,
staging`.`ITEM_CODE` AS `ITEM_CODE`,
staging`.`PRICE` AS `PRICE`,
staging`.`PROCESS_FEE`
staging`.`SHIPPING_FEE` AS `SHIPPING_FEE`,
staging`.`MESSAGE_ENGLISH` AS `MESSAGE_ENGLISH`,
staging`.`MESSAGE_SPANISH` AS `MESSAGE_SPANISH`,
staging`.`HEADER_ENGLISH` AS `HEADER_ENGLISH`,
staging`.`HEADER_SPANISH` AS `HEADER_SPANISH`,
staging`.`TOTAL_GIFT_CARD_AMT` AS `TOTAL_GIFT_CARD_AMT`,
staging`.`SIGNATURE_REQUIRED` AS `SIGNATURE_REQUIRED`,
staging`.`FROM_NAME` AS `FROM_NAME`,
staging`.`FROM_ADDRESS_1` AS `FROM_ADDRESS_1`,
staging`.`FROM_ADDRESS_2` AS `FROM_ADDRESS_2`,
staging`.`FROM_CITY` AS `FROM_CITY`,
staging`.`FROM_STATE` AS `FROM_STATE`,
staging`.`FROM_ZIP` AS `FROM_ZIP`,
staging`.`REASON_CD` AS `REASON_CD`,
staging`.`ACTION_CD` AS `ACTION_CD`,
staging`.`CALL_ID` AS `CALL_ID`,
staging`.`CUSTOM_1` AS `CUSTOM_1`,
staging`.`CUSTOM_2` AS `CUSTOM_2`,
staging`.`CUSTOM_4` AS `CUSTOM_3`,
staging`.`CUSTOM_4` AS `CUSTOM_4`,
staging`.`CUSTOM_5` AS `CUSTOM_5`,
staging`.`CUSTOM_6` AS `CUSTOM_6`,
staging`.`NGC_ORDER_NUMBER` AS `NGC_ORDER_NUMBER`
============================================
batches`.`BATCH_ID` AS `BATCH_ID`,
batches`.`CUSTOMER_ID` AS `CUSTOMER_ID`,
batches`.`BATCH_RECEIVED_DATE` AS `BATCH_RECEIVED_DATE`,
batches`.`BATCH_FILE_NAME` AS `BATCH_FILE_NAME`,
batches`.`BATCH_FILE_PATH` AS `BATCH_FILE_PATH`,
batches`.`BATCH_PREPARED` AS `BATCH_PREPARED`,
batches`.`BATCH_RECEIVED_NOTIFIED` AS `BATCH_RECEIVED_NOTIFIED`,
batches`.`BATCH_PRODUCTION_COMPLETED` AS `BATCH_PRODUCTION_COMPLETED`,
batches`.`BATCH_FULFILLMENT_COMPLETED` AS `BATCH_FULFILLMENT_COMPLETED`,
batches`.`BATCH_QC_COMPLETED` AS `BATCH_QC_COMPLETED`,
batches`.`BATCH_REJECTED` AS `BATCH_REJECTED`,
===========================================================
giftcards`.`NGC_ORDER_ID` AS `NGC_ORDER_ID`,
giftcards`.`CARD_COUNT_ID` AS `CARD_COUNT_ID`,
giftcards`.`CUST_ORDER_ID` AS `CUST_ORDER_ID`,
giftcards`.`CUST_SKU` AS `CUST_SKU`,
giftcards`.`CUST_DESCRIPTION` AS `CUST_DESCRIPTION`,
giftcards`.`SHIPPING_METHOD` AS `SHIPPING_METHOD`,
giftcards`.`EMAIL_ADDRESS` AS `EMAIL_ADDRESS`,
giftcards`.`MAILING_ADDRESS` AS `MAILING_ADDRESS`,
giftcards`.`NGC_SKU` AS `NGC_SKU`,
giftcards`.`NGC_DESCRIPTION` AS `NGC_DESCRIPTION`,
giftcards`.`LOB_ID` AS `LOB_ID`,
giftcards`.`LOB_ORDER_ID` AS `LOB_ORDER_ID`,
giftcards`.`CARD_NUMBER` AS `CARD_NUMBER`,
giftcards`.`TRACKING_NUMBER` AS `TRACKING_NUMBER`,
giftcards`.`SHIP_DATE` AS `SHIP_DATE`,



            </textarea>

        <!--</div>-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal end -->



<!--header.tpl end-->



