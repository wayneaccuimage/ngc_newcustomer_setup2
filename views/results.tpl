<!-- results.tpl -->
<div class="col-sm-11">
    %if defined("error"):
        <div class="panel panel-danger">
            <div class="panel-heading">
                <b><a id="hideShowLinkErrorResults" href="javascript:toggle('hideShowDivErrorResults','hideShowLinkErrorResults');" >  Hide </a></b>
            </div>
            <div id='hideShowDivErrorResults' class="panel-body" style="display: block;">
                <div class="alert alert-danger">
                    <p>{{ error }}</p>
                    %for message in data.status_message:
                        <p>{{ message }}</p>
                    %end
                </div>
            </div>
         </div>
    %end
    
    %if defined("success"):
        <div class="panel panel-success">
            <div class="panel-heading">
                <b><a id="hideShowLinkResults" href="javascript:toggle('hideShowDivResults','hideShowLinkResults');" >  Hide </a> Success Results</b>
            </div>
            <div id='hideShowDivResults' class="panel-body" style="display: block;">
                <div class="alert alert-success">
                    <p>{{ success }}</p>
                        %for message in data.status_message:
                            <p>{{ message }}</p>
                        %end
                </div>
            </div>
        </div>
    %end
</div>
<!-- results.tpl end -->