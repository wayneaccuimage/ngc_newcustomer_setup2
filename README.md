HOW To Run
-----------

Note - Please complete below installation steps, before trying to run the application

Follow below commands in command prompt to start the app

c:\> cd <PROJECT_DIR>/app

c:\> python server.py




Installation (Dependencies)
------------------------------

Step 1 (python)
---------------

Install python from

For 32-bit:
https://www.python.org/ftp/python/2.7.6/python-2.7.6.msi

For 64-bit:
https://www.python.org/ftp/python/2.7.6/python-2.7.6.amd64.msi

Note - After installation, add python to your PATH


Step 2 (pip)
--------------
1. Open "Command Prompt" as Administrator
2. Run Command
    c:\> cd <PROJECT_DIR>
    c:\> python get_pip.py

Note :- If running "python" throws an error, make sure python has been set in your PATH


Step 3 (dependencies)
--------------------
1. Open "Command Prompt"
2. Run Command
    c:\> cd <PROJECT_DIR>
    c:\> python install.py

Don't forget to install MySQL connector for Python
Run at http://localhost:8080/test/customer-setup


