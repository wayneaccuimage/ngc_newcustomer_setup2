import logging
import os

from mysql import connector

from app.server import config


logger = logging.getLogger(__name__)

# default environment
env = "test"


class DBUtil:
    def __init__(self, schema, environment=None):
        if environment is None:
            environment = env

        section = "database" + "_" + environment
        logger.info("--- using db - " + environment)

        self.host = config.get(section, "host")
        self.port = config.get(section, "port")
        self.user = config.get(section, "user")
        self.pwd = config.get(section, "password")
        self.db = config.get(section, schema)
        self.environment = environment

    def execute(self, query):
        logger.info('--- executing in mysql ---')
        logger.info('--- environment - ' + self.environment)

        con = None
        try:
            con = connector.connect(host=self.host,
                                    port=self.port,
                                    user=self.user,
                                    passwd=self.pwd,
                                    db=self.db)
            cur = con.cursor()

            logger.info('sql - ' + query)
            cur.execute(query)

            con.commit()
        except connector.Error, e:
            logger.info("Error %s:" % e.args[0])
            raise Exception(e)
        finally:
            if con:
                con.close()

    def find_all(self, query):
        logger.info('--- executing in mysql ---')
        logger.info('--- environment - ' + self.environment)

        con = None
        try:
            con = connector.connect(host=self.host,
                                    port=self.port,
                                    user=self.user,
                                    passwd=self.pwd,
                                    db=self.db)
            cur = con.cursor(cursor_class=MySQLCursorDict)

            logger.info('sql - ' + query)
            cur.execute(query)

            return cur.fetchall()
        except connector.Error, e:
            logger.error("Error %s:" % e)
            raise Exception(e)
        finally:
            if con:
                con.close()


class MySQLCursorDict(connector.cursor.MySQLCursor):
    def _row_to_python(self, rowdata, desc=None):
        row = super(MySQLCursorDict, self)._row_to_python(rowdata, desc)
        if row:
            return dict(zip(self.column_names, row))
        return None


def create_directory(path):
    if not os.path.isdir(path):
        try:
            os.makedirs(path)
        except Exception as e:
            logger.error("Error No directory access:" % e)
            raise Exception("Error No directory access:" + e)