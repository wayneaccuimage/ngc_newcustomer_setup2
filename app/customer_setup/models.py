import ConfigParser
import os
import logging
import datetime

from mysql.connector import conversion

from app.server import config
from utils import DBUtil
import utils


logger = logging.getLogger(__name__)


def _token(key):
    return "<" + key + ">"


def _value(key, value, single_quotes=True, escape_characters=True):
    if value is None:
        return "NULL"

    if key == "id":
        return value

    if escape_characters:
        value = conversion.MySQLConverter().escape(value)

    if single_quotes:
        return "'" + value + "'"
    else:
        return value


def _fill_query_values(query, object=None, filters=None, surround_quotes=True):
    updated_qry = query

    if object is not None:
        logging.debug(vars(object).keys())
        for k in vars(object).keys():
            updated_qry = updated_qry.replace(_token(k), _value(k, object.__dict__[k], surround_quotes))

    if filters is not None:
        for k, v in filters.items():
            updated_qry = updated_qry.replace(_token(k), _value(k, str(v), surround_quotes))

    return updated_qry


def _next_id(schema, query):
        db_util = DBUtil(schema=schema)

        id_query = _fill_query_values(query)
        logger.info('DEF _next_id(schema, query) - select max id qry - ' + id_query)
        results = db_util.find_all(id_query)

        for row in results:
            max_id = row.get("MAXID")
            break

        if max_id is None:
            return max_id

        next_id = int(max_id) + 1
        return str(next_id)


_CUSTOMER_INFO_EXTENSION = ".cfg"

class ResultMessages:
    def __init__(self):
        self.msgList = []

    def setMsg(self, msg):
        self.msgList.__add__(msg)

class CustomerInfo:
    def __init__(self):
        self.name = None
        self.full_name = None
        self.date = None
        self.ftp_user_name = None
        self.ftp_pwd = None
        self.ftp_incoming_directory = None
        self.ftp_outgoing_directory = None
        self.env = None

    def set_name(self, name, full_name=None):
        # defaults
        self.name = name

        if full_name is None:
            self.full_name = name

        # set todays date as default
        self.date = datetime.date.today().strftime("%Y-%m-%d")
        self.ftp_incoming_directory = \
            config.get(utils.env, "ftp_incoming_directory_prefix") + name + os.path.sep + "incoming" + os.path.sep
        self.ftp_outgoing_directory = \
            config.get(utils.env, "ftp_outgoing_directory_prefix") + name + os.path.sep + "outgoing" + os.path.sep

    @classmethod
    def _path_to_appdata(cls, env=None):
        if env is None:
            env = utils.env

        logger.info("Class CustomerInfo DEF _path_to_appdata - path - " + os.path.dirname(__file__))
        # appdata
        path_to_appdata = os.path.abspath(
                            os.path.join(os.path.dirname(__file__),
                                         ".." + os.path.sep,
                                         ".." + os.path.sep,
                                         "appdata",
                                         env))

        return path_to_appdata

    @classmethod
    def load(cls, customer_name, env=None):
        # appdata
        if env is not None:
            path_to_appdata = cls._path_to_appdata(env)
        else:
            path_to_appdata = cls._path_to_appdata()

        filename = os.path.join(path_to_appdata + os.path.sep, customer_name + _CUSTOMER_INFO_EXTENSION)
        if os.path.isfile(filename):
            config = ConfigParser.ConfigParser()
            config.readfp(open(filename))

            customer_info = CustomerInfo()

            customer_info.name = config.get("customer_info", "name")

            if config.has_option("customer_info", "full_name"):
                customer_info.full_name = config.get("customer_info", "full_name")

            customer_info.date = config.get("customer_info", "date")
            customer_info.ftp_user_name = config.get("customer_info", "ftp_user_name")
            customer_info.ftp_pwd = config.get("customer_info", "ftp_pwd")
            customer_info.ftp_incoming_directory = config.get("customer_info", "ftp_incoming_directory")
            customer_info.ftp_outgoing_directory = config.get("customer_info", "ftp_outgoing_directory")
            customer_info.env = config.get("customer_info", "env")

            return customer_info

        return None

    @classmethod
    def write(cls, customer_info):
        config = ConfigParser.ConfigParser()
        config.add_section("customer_info")
        config.set("customer_info", "name", customer_info.name)
        config.set("customer_info", "full_name", customer_info.full_name)
        config.set("customer_info", "date", customer_info.date)
        config.set("customer_info", "ftp_user_name", customer_info.ftp_user_name)
        config.set("customer_info", "ftp_pwd", customer_info.ftp_pwd)
        config.set("customer_info", "ftp_incoming_directory", customer_info.ftp_incoming_directory)
        config.set("customer_info", "ftp_outgoing_directory", customer_info.ftp_outgoing_directory)
        config.set("customer_info", "env", customer_info.env)

        # appdata
        path_to_appdata = cls._path_to_appdata()

        filename = os.path.join(path_to_appdata + os.path.sep, customer_info.name + _CUSTOMER_INFO_EXTENSION)
        if os.path.isfile(filename):
            # update config
            config.write(open(filename, mode='wb'))
        else:
            config.write(open(filename, mode='wb'))



CREATE_QUERY = """
INSERT INTO validationupdate
(ID,DBID,UNIQUEID,FILECONVERTTYPE,FIELDDELIMITER,NEWLINEDELIMITER,FIELDLIST,PREUPDATEQUERYLIST,POSTUPDATEQUERYLIST,
NEWTABLE,NEWDIRECTORY,SOURCEDIRECTORY,TASK,EXCEPTIONDIRECTORY,BACKUPDIRECTORY,EMAILSUCCESSTOLIST,EMAILERRORTOLIST,
CHECKDATEFORMAT,HEADERLINECOUNT,FOOTERLINECOUNT,FILEACTION,FILEPREFIX,NEWLINEMULTI,TEMPTABLE,TRANSFERDIRECTORYLIST,
REMOVECHARACTER,FILETABLENAME,XX_EMAILCUSTOMERTOLIST,XX_FIELDLIST_BK,XX_POSTUPDATEQUERYLIST)

VALUES (<id>,
        <db_id>,
        <unique_id>,
        <file_convert_type>,
        <field_delimiter>,
        <new_line_delimiter>,
        <field_list>,
        NULL,
        <post_update_query_list>,
        <new_table>,
        <new_directory>,
        NULL,
        <task>,
        <exception_directory>,
        <backup_directory>,
        <email_success_to_list>,
        <email_error_to_list>,
        '0',
        '1',
        '0',
        'MOVE',
        NULL,
        '0',
        NULL,
        NULL,
        <remove_character>,
        '',
        'william@ngc-group.com',
        NULL,
        NULL);
"""

UPDATE_QUERY = """
UPDATE validationupdate
SET
    DBID = <db_id>,
    UNIQUEID = <unique_id>,
    FILECONVERTTYPE = <file_convert_type>,
    FIELDDELIMITER = <field_delimiter>,
    NEWLINEDELIMITER = <new_line_delimiter>,
    FIELDLIST = <field_list>,
    POSTUPDATEQUERYLIST = <post_update_query_list>,
    NEWTABLE = <new_table>,
    NEWDIRECTORY = <new_directory>,
    TASK = <task>,
    EXCEPTIONDIRECTORY = <exception_directory>,
    BACKUPDIRECTORY = <backup_directory>,
    EMAILSUCCESSTOLIST = <email_success_to_list>,
    EMAILERRORTOLIST = <email_error_to_list>,
    REMOVECHARACTER = <remove_character>
WHERE
    ID = <id>;
"""

POST_UPDATE_QUERY_LIST_PRETTY = """
INSERT INTO BATCHES
(BATCH_ID,CUSTOMER_ID,BATCH_FILE_NAME,BATCH_FILE_PATH,BATCH_RECEIVED_DATE,BATCH_PRODUCTION_COMPLETED,
BATCH_FULFILLMENT_COMPLETED,BATCH_QC_COMPLETED)

VALUES (ASSIGNED_BATCH_ID,
        '<name>',
        'ASSIGNED_FILE_NAME',
        'ASSIGNED_BACKUP_FILE_PATH',
        Now(),
        0,
        0,
        0);
"""

# removing all formatting from above query
POST_UPDATE_QUERY_LIST = POST_UPDATE_QUERY_LIST_PRETTY.replace("\n", "").replace("        ", "")


VALIDATION_UPDATE_CREATE_BACKUP_TABLE = """
CREATE TABLE <backup_table_name> LIKE validationupdate;
"""

VALIDATION_UPDATE_COPY_DATA_BACKUP_TABLE = """
INSERT <backup_table_name> SELECT * FROM validationupdate;
"""


SELECT_ALL_QUERY = """
SELECT *
FROM validationupdate
WHERE ID < 999
ORDER BY ID DESC;
"""

SELECT_BY_ID_QUERY = """
SELECT *
FROM validationupdate
WHERE
  ID = <id>
"""

SELECT_MAX_ID_QUERY = """
SELECT MAX(ID) AS MAXID
FROM validationupdate
WHERE ID < 999
"""

SELECT_BY_DB_ID_QUERY = """
SELECT *
FROM validationupdate
WHERE
  DBID = '<db_id>'
"""


class ValidationUpdate:
    def __init__(self):
        self.id = None
        self.db_id = None
        self.unique_id = None
        self.file_convert_type = None
        self.field_delimiter = None
        self.new_line_delimiter = None
        self.field_list = None
        self.post_update_query_list = None
        self.new_table = None
        self.new_directory = None
        self.task = None
        self.exception_directory = None
        self.backup_directory = None
        self.email_success_to_list = None
        self.email_error_to_list = None
        self.remove_character = None

    @classmethod
    def _convert_row_to_model(cls, row):
        """Build the page model"""
        vu = ValidationUpdate()
        vu.id = row.get("ID")
        vu.db_id = row.get("DBID")
        if vu.db_id:
            vu.db_id = str(vu.db_id).strip()

        vu.unique_id = row.get("UNIQUEID")
        vu.file_convert_type = row.get("FILECONVERTTYPE")
        vu.field_delimiter = row.get("FIELDDELIMITER")
        vu.new_line_delimiter = row.get("NEWLINEDELIMITER")
        vu.field_list = row.get("FIELDLIST")
        vu.post_update_query_list = row.get("POSTUPDATEQUERYLIST")
        vu.new_table = row.get("NEWTABLE")
        vu.new_directory = row.get("NEWDIRECTORY")
        vu.task = row.get("TASK")
        vu.exception_directory = row.get("EXCEPTIONDIRECTORY")
        vu.backup_directory = row.get("BACKUPDIRECTORY")
        vu.email_success_to_list = row.get("EMAILSUCCESSTOLIST")
        vu.email_error_to_list = row.get("EMAILERRORTOLIST")
        vu.remove_character = row.get("REMOVECHARACTER")

        return vu

    @classmethod
    def is_exists(cls, db_id):
        list = cls.find_by_dbid(db_id)

        if len(list) == 0:
            return False # not exists
        else:
            return True # exists

    @classmethod
    def create(cls, validation_update):
        if cls.is_exists(validation_update.db_id):
            logger.info("DEF create(cls, validation_update) - validation update row already created, ignoring create")

            list = cls.find_by_dbid(validation_update.db_id)
            validation_update = list[0]
            return

        db_util = DBUtil(schema="schema_dvofficeupdate")

        id_query = _fill_query_values(SELECT_MAX_ID_QUERY)
        logger.info('select max id qry - ' + id_query)
        results = db_util.find_all(id_query)

        for row in results:
            max_id = row.get("MAXID")
            break

        next_id = int(max_id) + 1
        validation_update.id = str(next_id)

        query = _fill_query_values(CREATE_QUERY, object=validation_update)
        logger.info('create query - ' + query)

        db_util.execute(query)

    @classmethod
    def update(cls, validation_update):
        # check all properties are set
        query = _fill_query_values(UPDATE_QUERY, object=validation_update)
        logger.info('MODELS DEF update(cls, validation_update) - update query - ' + query)

        db_util = DBUtil(schema="schema_dvofficeupdate")
        db_util.execute(query)

    @classmethod
    def backup_table(cls):
        # create backup
        db_util = DBUtil(schema="schema_dvofficeupdate")

        backup_table_name = "validationupdate_" + datetime.datetime.now().strftime("cs%Y%m%d_%H%M%S")

        create_table_query = _fill_query_values(VALIDATION_UPDATE_CREATE_BACKUP_TABLE,
                                                filters={"backup_table_name": backup_table_name},
                                                surround_quotes=False)
        logger.info('create backup table query - ' + create_table_query)
        db_util.execute(create_table_query)

        insert_backup_data = _fill_query_values(VALIDATION_UPDATE_COPY_DATA_BACKUP_TABLE,
                                                filters={"backup_table_name": backup_table_name},
                                                surround_quotes=False)

        logger.info('copying data to backup table query - ' + insert_backup_data)
        db_util.execute(insert_backup_data)

    @classmethod
    def find_by_id(cls, id):
        query = _fill_query_values(SELECT_BY_ID_QUERY, filters={ "id": id })

        db_util = DBUtil(schema="schema_dvofficeupdate")
        results = db_util.find_all(query)

        if len(results) > 0:
            # first row
            vu = cls._convert_row_to_model(results[0])
            return vu

    @classmethod
    def find_all(cls):
        query = _fill_query_values(SELECT_ALL_QUERY)

        db_util = DBUtil(schema="schema_dvofficeupdate")
        results = db_util.find_all(query)

        list = []
        for row in results:
            vu = cls._convert_row_to_model(row)
            list.append(vu)

        return list

    @classmethod
    def find_by_dbid(cls, db_id, env=None):
        if env is None:
            env = utils.env

        query = _fill_query_values(SELECT_BY_DB_ID_QUERY,
                                   filters={ "db_id": db_id },
                                   surround_quotes=False)

        db_util = DBUtil(schema="schema_dvofficeupdate", environment=env)
        results = db_util.find_all(query)

        list = []
        for row in results:
            vu = cls._convert_row_to_model(row)
            list.append(vu)

        return list


DVWEBINDEX_SETTINGS_INSERT_QUERY = """
INSERT INTO dvwebindex_settings (JOB,SearchButtonTable,JobDatabase,ColumnWidth,AutoSubmit,ShowAppendRadioButton,
BarcodeName,BarcodeAppendMessage,ReadAppendBarcodeFromFileName,TurnValidationOn,ValidationView,ViewAsPDF,
ViewAsPDFExt,DisplaySplitPDFButton,AllowModify,ModifySummaryOrderBy,APIOrderBy,ModifyEdejaviewLink,ReadBarcode,
BarcodePrefix,ConvertToTextPDF,GetOnlyTextFile,OCRPageCount,DisplayTextFile,TextFieldName,ShowFormSelection,
UseAccuZip,SplitPDF,GroupIndexing,GroupEndPosition,DisplayConvertPDFButton,ShowTwoFrameLayoutForIndexing,
FileFrameWidthSetting,ImageFrameWidthSetting,IndexFrameWidthSetting,FilesCheckBoxDisplay,DisplayMergePDFButton,
DisplayOCRPDFButton,ValidationViewLimit,ValidationViewAllowEmptySearch,ConvertPDF,UseForValidationFileUpload,
AllowModifyForCertainNonAdminUsers,DisplayDuplicateButton,DisableModifyForNonAdminUsers,DisplayRenameButton,
ExportView,ExportTable,NoModifyDeleteButton,API_DBID,LeaveZipAlone)

VALUES
    ('<customer_name>',
     'search_fieldinfo_<table_name>',
     null,null,0,0,null,null,0,0,'db_batches_staging_giftcards',0,null,0,0,null,'',null,0,null,0,0,null,0,
     null,0,0,0,0,null,0,0,null,null,null,0,0,0,0,null,0,1,null,0,0,1,null,null,0,null,0);
"""

DVWEBINDEX_SETTINGS_SELECT_FIND_SEARCH_BUTTON_TABLE_QUERY = """
SELECT SEARCHBUTTONTABLE FROM dvwebindex_settings
WHERE LOWER(JOB) = <customer_name>;
"""

DVWEBINDEX_SETTINGS_CREATE_BACKUP_TABLE = """
CREATE TABLE <backup_table_name> LIKE dvwebindex_settings;
"""

DVWEBINDEX_SETTINGS_COPY_DATA_BACKUP_TABLE = """
INSERT <backup_table_name> SELECT * FROM dvwebindex_settings;
"""


class DVWebIndexFileUpload:
    def __init__(self):
        pass

    @classmethod
    def add_row_dvwebindex_settings(cls, customer_name):

        resultMsgs = ResultMessages()

        db_util = DBUtil(schema="schema_dvwebindex_fileupload")

        query = _fill_query_values(DVWEBINDEX_SETTINGS_INSERT_QUERY,
                                   filters= {"customer_name": customer_name,
                                             "table_name": str(customer_name).lower()},
                                   surround_quotes=False)
        logger.info("DVWEBINDEX_SETTINGS insert query - " + query)
        db_util.execute(query)

    @classmethod
    def backup_table(cls):
        # create backup
        db_util = DBUtil(schema="schema_dvwebindex_fileupload")

        backup_table_name = "dvwebindex_settings_" + datetime.datetime.now().strftime("cs%Y%m%d_%H%M%S")

        create_table_query = _fill_query_values(DVWEBINDEX_SETTINGS_CREATE_BACKUP_TABLE,
                                                filters={"backup_table_name": backup_table_name},
                                                surround_quotes=False)
        logger.info('create backup table query - ' + create_table_query)
        db_util.execute(create_table_query)

        insert_backup_data = _fill_query_values(DVWEBINDEX_SETTINGS_COPY_DATA_BACKUP_TABLE,
                                                filters={"backup_table_name": backup_table_name},
                                                surround_quotes=False)

        logger.info('copying data to backup table query - ' + insert_backup_data)
        db_util.execute(insert_backup_data)

    @classmethod
    def find_search_field_info_table(cls, customer_name, env=None):
        if env is None:
            env = utils.env

        db_util = DBUtil(schema="schema_dvwebindex_fileupload", environment=env)

        query = _fill_query_values(DVWEBINDEX_SETTINGS_SELECT_FIND_SEARCH_BUTTON_TABLE_QUERY,
                                   filters={"customer_name": str(customer_name).lower()})

        logger.info("DVWEBINDEX_SETTINGS search field info table - " + query)
        results = db_util.find_all(query)

        for row in results:
            table_name = row.get("SEARCHBUTTONTABLE")
            return table_name



SEARCH_FIELDINFO_MASTER_TABLE_EXISTS_QUERY = """
SHOW TABLES LIKE 'search_fieldinfo_master';
"""

SEARCH_FIELDINFO_MASTER_COPY_STRUCTURE_QUERY = """
CREATE TABLE <table_name> LIKE search_fieldinfo_master;
"""

SEARCH_FIELDINFO_TABLE_EXISTS_QUERY = """
SHOW TABLES LIKE '<table_name>';
"""

SEARCH_FIELDINFO_INSERT_QUERY = """
INSERT INTO <table_name>
(DISPLAY_FIELDNAME,FIELDNAME,INPUTTYPE,SEARCH_NAMEDORDER,DISPLAY_NAMEDORDER,PREDEFINED,PREDEFINED_QUERY,
PREDEFINED_ORDER,CONSTANT_VALUE,PREDEFINED_SAY_ALLVALUES)
VALUES
('<display_field_name>',
 '<field_name>',
 'text',
 <search_name_order>,
 <display_name_order>,0,null,null,null,0);
"""

SEARCH_FIELDINFO_INSERT_BATCH_QUERY = """
INSERT INTO <table_name>
(DISPLAY_FIELDNAME,FIELDNAME,INPUTTYPE,SEARCH_NAMEDORDER,DISPLAY_NAMEDORDER,PREDEFINED,PREDEFINED_QUERY,
PREDEFINED_ORDER,CONSTANT_VALUE,PREDEFINED_SAY_ALLVALUES)
values
    ('BatchID', 'BATCH_ID', 'text', '98', null, '1',
     '<batch_query>', NULL, NULL, '1');
"""

SEARCH_FIELDINFO_INSERT_BATCH_SUB_QUERY_PRETTY = """
select Batch_ID AS PREDEFINEDVALUES, concat(Batch_ID,' : ',Batch_File_Name) as DESCRIPTION from db_batches where customer_id ='|||CURRENTJOBID|||'
"""

SEARCH_FIELDINFO_INSERT_BATCH_SUB_QUERY = SEARCH_FIELDINFO_INSERT_BATCH_SUB_QUERY_PRETTY.replace("\n", "")

SEARCH_FIELDINFO_INSERT_CUSTOMER_ID_QUERY = """
INSERT INTO <table_name> (DISPLAY_FIELDNAME,FIELDNAME,INPUTTYPE,SEARCH_NAMEDORDER,
DISPLAY_NAMEDORDER,PREDEFINED,PREDEFINED_QUERY,PREDEFINED_ORDER,CONSTANT_VALUE,PREDEFINED_SAY_ALLVALUES)
VALUES ('CustomerID','CUSTOMER_ID','text',99,null,0,null,null,'|||CURRENTJOBID|||',0);
"""


SEARCH_FIELDINFO_UPDATE_QUERY = """
UPDATE <table_name>
SET
    SEARCH_NAMEDORDER = <search_name_order>,
    DISPLAY_NAMEDORDER = <display_name_order>
WHERE
    DISPLAY_FIELDNAME = '<display_field_name>'
AND FIELDNAME = '<field_name>'
"""

SEARCH_FIELDINFO_DISPLAY_NAME_LIST_QUERY = """
SELECT FIELDNAME, DISPLAY_FIELDNAME
FROM <table_name>
WHERE (search_namedorder != 98
AND search_namedorder != 99)
OR search_namedorder is NULL
"""

SEARCH_FIELDINFO_DELETE_QUERY = """
DELETE
FROM <table_name>
"""

SEARCH_FIELDINFO_CREATE_BACKUP_TABLE = """
CREATE TABLE <backup_table_name> LIKE <table_name>;
"""

SEARCH_FIELDINFO_COPY_DATA_BACKUP_TABLE = """
INSERT <backup_table_name> SELECT * FROM <table_name>;
"""


class SearchFieldInfo:
    def __init__(self):
        self.display_field_name = None
        self.field_name = None
        self.input_type = None
        self.search_name_order = None
        self.display_name_order = None

    @classmethod
    def search_info_table_exists(cls, customer_name, env=None):
        if env is None:
            env = utils.env

        db_util = DBUtil(schema="schema_dvwebindex_fileupload", environment=env)

        table_name = DVWebIndexFileUpload.find_search_field_info_table(customer_name, env)

        table_exists_query = _fill_query_values(SEARCH_FIELDINFO_TABLE_EXISTS_QUERY,
                                                filters={"table_name": str(table_name).lower()},
                                                surround_quotes=False)

        list = db_util.find_all(table_exists_query)

        if len(list) == 0:
            return False # not exists
        else:
            return True # exists

    @classmethod
    def _master_table_exists(cls):
        db_util = DBUtil(schema="schema_dvwebindex_fileupload")

        table_exists_query = _fill_query_values(SEARCH_FIELDINFO_MASTER_TABLE_EXISTS_QUERY)
        list = db_util.find_all(table_exists_query)

        if len(list) == 0:
            return False # not exists
        else:
            return True # exists

    @classmethod
    #def create(cls, customer_name, search_field_info_list):
    def create(cls, customer_setup):
        db_util = DBUtil(schema="schema_dvwebindex_fileupload")

        table_exist = cls.search_info_table_exists(customer_setup.customer_info.name)

        if not table_exist:
            master_table_exists = cls._master_table_exists()
            if not master_table_exists:
                raise Exception("No Master table found for Search Field Info")
            # TODO camelcase name
            table_name = DVWebIndexFileUpload.find_search_field_info_table(customer_setup.customer_info.name)

            # copy table structure
            copy_structure_query = _fill_query_values(SEARCH_FIELDINFO_MASTER_COPY_STRUCTURE_QUERY,
                                                      filters={"table_name": str(table_name).lower()},
                                                      surround_quotes=False)
            logger.info("Copy structure search_fieldinfo_master_copy query - " + copy_structure_query)
            db_util.execute(copy_structure_query)


            cls.insert(table_name, customer_setup)
            cls.insert_98_99(table_name)
        else:
            logger.info("Table: search_fieldinfo_"+customer_setup.customer_name+" has been already created")

            table_name = DVWebIndexFileUpload.find_search_field_info_table(customer_setup.customer_name)

            # clean all records
            cls.delete(table_name)
            cls.insert(table_name, customer_setup.search_field_info_list)
            cls.insert_98_99(table_name)

    @classmethod
    def insert(cls, table_name, customer_setup):
        """

        :type cls: object
        """
        db_util = DBUtil(schema="schema_dvwebindex_fileupload")

        cls.add_required_fields(customer_setup)

        for search_field_info in customer_setup.search_field_info_list:
            search_name_order = search_field_info.search_name_order if search_field_info.search_name_order else "null"
            display_name_order = search_field_info.display_name_order if search_field_info.display_name_order else "null"
            f = {
                "table_name": str(table_name).lower(),
                "display_field_name": search_field_info.display_field_name,
                "field_name": search_field_info.field_name,
                "search_name_order": search_name_order,
                "display_name_order": display_name_order
            }
            insert_query = _fill_query_values(SEARCH_FIELDINFO_INSERT_QUERY,
                                              filters=f,
                                              surround_quotes=False)

            logger.info("insert search_field_info query - " + insert_query)
            db_util.execute(insert_query)

    @classmethod
    def add_required_fields(cls, customer_setup):
        requiredFieldsMsg = ''
        if cls.inSearchFieldInfoList(customer_setup,"CARD_NUMBER"):
            pass
        else:
            cn = SearchFieldInfo()
            cn.field_name = "CARD_NUMBER"
            cn.display_field_name = "Card Number"
            cn.input_type = "text"
            cn.search_name_order = 95
            cn.display_name_order = 95
            customer_setup.search_field_info_list.append(cn)
            requiredFieldsMsg = "CARD_NUMBER"

        if cls.inSearchFieldInfoList(customer_setup, "TRACKING_NUMBER"):
            pass
        else:
            cn = SearchFieldInfo()
            cn.field_name = "TRACKING_NUMBER"
            cn.display_field_name = "Tracking Number"
            cn.input_type = "text"
            cn.search_name_order = 96
            cn.display_name_order = 96
            customer_setup.search_field_info_list.append(cn)
            requiredFieldsMsg += " - TRACKING_NUMBER"

        if cls.inSearchFieldInfoList(customer_setup, "SHIP_DATE"):
            pass
        else:
            cn = SearchFieldInfo()
            cn.field_name = "SHIP_DATE"
            cn.display_field_name =  "Ship Date"
            cn.input_type = "text"
            cn.search_name_order = 97
            cn.display_name_order = 97
            customer_setup.search_field_info_list.append(cn)
            requiredFieldsMsg += " - SHIP_DATE"

        if requiredFieldsMsg != '':
            customer_setup.status_message.append(" - Added additional required field(s): " + requiredFieldsMsg)
            logger.info(requiredFieldsMsg)

    @classmethod
    def inSearchFieldInfoList(cls,customer_setup, field_name):
        for search_field_info in customer_setup.search_field_info_list:
            if search_field_info.field_name == field_name:
                return True
        else:
            return False

    @classmethod
    def insert_98_99(cls, table_name):
        db_util = DBUtil(schema="schema_dvwebindex_fileupload")

        batch_query = _fill_query_values(SEARCH_FIELDINFO_INSERT_BATCH_QUERY,
                                   filters={"table_name": str(table_name).lower(),
                                            "batch_query": SEARCH_FIELDINFO_INSERT_BATCH_SUB_QUERY},
                                   surround_quotes=False)
        logger.info("DEF add_required_fields(cls, customer_setup): insert search_field_info 98, 99 batch query - " + batch_query)
        db_util.execute(batch_query)

        customer_id_query = _fill_query_values(SEARCH_FIELDINFO_INSERT_CUSTOMER_ID_QUERY,
                                    filters={"table_name": str(table_name).lower()},
                                    surround_quotes=False)
        logger.info("DEF add_required_fields(cls, customer_setup): insert search_field_info 98, 99 customer_id query - " + customer_id_query)
        db_util.execute(customer_id_query)

    @classmethod
    def update(cls, customer_setup):
        table_name = DVWebIndexFileUpload.find_search_field_info_table(customer_setup.customer_info.name)

        # clean all records
        cls.delete(table_name)
        cls.insert(table_name, customer_setup)
        cls.insert_98_99(table_name)

    @classmethod
    def delete(cls, table_name):
        db_util = DBUtil(schema="schema_dvwebindex_fileupload")

        delete_query = _fill_query_values(SEARCH_FIELDINFO_DELETE_QUERY,
                                          filters={"table_name": str(table_name).lower()},
                                          surround_quotes=False)

        logger.info("delete search_field_info query - " + delete_query)
        db_util.execute(delete_query)

    @classmethod
    def backup_table(cls, customer_name):
        # create backup
        db_util = DBUtil(schema="schema_dvwebindex_fileupload")

        table_name = DVWebIndexFileUpload.find_search_field_info_table(customer_name)

        backup_table_name = str(table_name).lower() + "_" + datetime.datetime.now().strftime("cs%Y%m%d_%H%M%S")

        create_table_query = _fill_query_values(SEARCH_FIELDINFO_CREATE_BACKUP_TABLE,
                                                filters={
                                                    "backup_table_name": backup_table_name,
                                                    "table_name": table_name
                                                },
                                                surround_quotes=False)
        logger.info('create backup table query - ' + create_table_query)
        db_util.execute(create_table_query)

        insert_backup_data = _fill_query_values(SEARCH_FIELDINFO_COPY_DATA_BACKUP_TABLE,
                                                filters={
                                                    "backup_table_name": backup_table_name,
                                                    "table_name": table_name
                                                },
                                                surround_quotes=False)

        logger.info('copying data to backup table query - ' + insert_backup_data)
        db_util.execute(insert_backup_data)

    @classmethod
    def get_info_list_from(cls, field_list, display_name_list):
        field_list = field_list.strip()
        display_name_list = display_name_list.strip()

        f_list = field_list.split("\n")
        d_list = display_name_list.split("\n")
        if len(f_list) != len(d_list):
            raise Exception("field list and display list are not of same length")

        index = 0
        list = []
        for l_idx, field in enumerate(f_list):
            field_split = field.split(",")
            search_field_info = SearchFieldInfo()

            search_field_info.field_name = field_split[0]
            search_field_info.display_field_name = d_list[l_idx].strip()

            # default
            search_field_info.input_type = "text"
            if len(field_split) == 3 and "SKIP" not in field_split[1]:
                # set name orders to null, if field info is SKIP
                search_field_info.display_name_order = index + 1
                search_field_info.search_name_order = index + 1
                index += 1

            list.append(search_field_info)

        return list

    @classmethod
    def get_display_name_and_field_name_list(cls, customer_name, env=None):
        if env is None:
            env = utils.env

        db_util = DBUtil(schema="schema_dvwebindex_fileupload", environment=env)

        table_name = DVWebIndexFileUpload.find_search_field_info_table(customer_name, env)

        select_query = _fill_query_values(SEARCH_FIELDINFO_DISPLAY_NAME_LIST_QUERY,
                                          filters={"table_name": str(table_name).lower()},
                                          surround_quotes=False)

        logger.info('find table - ' + select_query)
        results = db_util.find_all(select_query)

        map = {}
        for row in results:
            map[row.get("FIELDNAME")] = row.get("DISPLAY_FIELDNAME")

        return map




JOBS_INSERT_QUERY = """
INSERT INTO jobs
(JOBID,DESCRIPTION,FTP_OUTGOING_PATH,PATH,FOLDER,IS_DIRECTORY,IS_VAL_UPLOAD_FOLDER,AIXPRESS_UPLOADER,
JAVASCRIPT_LINES_FOR_MODIFY,FTP_OUTGOING_PATH_2)
VALUES
(<name>,
 <name>,
 <ftp_outgoing_directory>,
 <ftp_incoming_directory>,null,0,1,1,null,null);
"""

JOBS_EXISTS_QUERY = """
SELECT *
FROM jobs
WHERE
    JOBID = <name>
AND DESCRIPTION = <name>
"""

JOBS_CREATE_BACKUP_TABLE = """
CREATE TABLE <backup_table_name> LIKE jobs;
"""

JOBS_COPY_DATA_BACKUP_TABLE = """
INSERT <backup_table_name> SELECT * FROM jobs;
"""


class Jobs:
    def __init__(self):
        pass

    @classmethod
    def create(cls, customer_info):
        db_util = DBUtil(schema="schema_dvwebindex_fileupload")

        exists_query = _fill_query_values(JOBS_EXISTS_QUERY,
                                          object=customer_info)
        logger.info("JOBS: exists query - " + exists_query)
        list = db_util.find_all(exists_query)

        # no update necessary as customer name should never change
        if len(list) == 0:
            query = _fill_query_values(JOBS_INSERT_QUERY,
                                       object=customer_info)
            logger.info("JOBS: insert query - " + query)
            db_util.execute(query)

    @classmethod
    def backup_table(cls):
        # create backup
        db_util = DBUtil(schema="schema_dvwebindex_fileupload")

        backup_table_name = "jobs_" + datetime.datetime.now().strftime("cs%Y%m%d_%H%M%S")

        create_table_query = _fill_query_values(JOBS_CREATE_BACKUP_TABLE,
                                                filters={"backup_table_name": backup_table_name},
                                                surround_quotes=False)
        logger.info('JOBS DEF backup_table: create backup table query - ' + create_table_query)
        db_util.execute(create_table_query)

        insert_backup_data = _fill_query_values(JOBS_COPY_DATA_BACKUP_TABLE,
                                                filters={"backup_table_name": backup_table_name},
                                                surround_quotes=False)

        logger.info('JOBS DEF backup_table: copying data to backup table query - ' + insert_backup_data)
        db_util.execute(insert_backup_data)


REPORTS_INSERT_QUERY = """
INSERT INTO reports
(REPORTID, NAME, TABLE_ID, COLUMNID, ROW_ID, RUNTABLEONCLICK, SQLSCRIPT, ON_CLICK_PASS_SQL, RUN_SINGLE_REPORT_ON_CLICK,
RUNTABLEONRIGHTCLICK, REPORTBREAKDOWN, LEFT_CLICK_PROMPT_FOR_CUSTOM_VALUE_TITLE,
LEFT_CLICK_PROMPT_FOR_CUSTOM_VALUE_DEFAULT, RIGHT_CLICK_PROMPT_FOR_CUSTOM_VALUE_TITLE,
RIGHT_CLICK_PROMPT_FOR_CUSTOM_VALUE_DEFAULT, SHOW_IMMEDIATE_FORM_ON_LEFT_CLICK, RUN_SINGLE_REPORT_ON_RIGHT_CLICK,
RESTRICTEDTOUSERTYPES, DRILLDOWNPARAMETER, SHOW_IMMEDIATE_FORM_ON_RIGHT_CLICK, REPORT_CELL_EXTRA, SHOW_CHECK_BOXES,
ON_CLICK_RESIZE_MASTER_FRAMESET, ON_CLICK_RESIZE_REPORTS_FRAMESET, ON_CLICK_RESIZE_SINGLEREPORT_FRAMESET, BUTTON_GROUP_ID)

VALUES
(<report_id>, <customer_name>, <table_id>, '1', <row_id>, '1',
<batch_subquery>,
<customer_subquery>, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
NULL, NULL, NULL, NULL, NULL, NULL, NULL)
"""

REPORTS_INSERT_BATCH_SUBQUERY = "SELECT BATCH_ID FROM DB_BATCHES WHERE CUSTOMER_ID = <customer_name> AND (BATCH_QC_COMPLETED IS NULL OR BATCH_QC_COMPLETED=0) AND (BATCH_REJECTED IS NULL OR BATCH_REJECTED=0)\r\n"

REPORTS_INSERT_CUSTOMER_SUBQUERY = "CUSTOMER_ID = <customer_name>"

REPORTS_SELECT_MAX_REPORT_ID_QUERY = """
SELECT MAX(REPORTID) AS MAXID
FROM reports
WHERE LOWER(NAME) LIKE '<start_letter>%'
AND REPORTID > 2100
"""

REPORTS_SELECT_MAX_ROW_ID_QUERY = """
SELECT MAX(ROW_ID) AS MAXID
FROM reports
WHERE LOWER(NAME) LIKE '<start_letter>%'
"""

REPORTS_SELECT_TABLE_ID_QUERY = """
SELECT RUNTABLEONCLICK
FROM reports
WHERE LOWER(name) = <start_letter>
AND table_id = 20
"""

REPORTS_SELECT_NAME_ASC_QUERY = """
SELECT * FROM reports WHERE reportid > 2100 AND LOWER(NAME) LIKE '<start_letter>%' ORDER BY NAME ASC;
"""

REPORTS_UPDATE_NAME_ASC_TEMP_REPORT_ID_QUERY = """
UPDATE reports SET reportid = <temp_report_id>, row_id = <new_row_id>
WHERE
    reportid = <report_id>
AND row_id = <row_id>
"""

REPORTS_UPDATE_NAME_ASC_REPORT_ID_QUERY = """
UPDATE reports SET reportid = <new_report_id>
WHERE
    reportid = <report_id>
"""

REPORTS_SELECT_NAME_EXISTS_QUERY = """
SELECT * FROM reports WHERE LOWER(name) = <customer_name>
"""

REPORTS_CREATE_BACKUP_TABLE = """
CREATE TABLE <backup_table_name> LIKE reports;
"""

REPORTS_COPY_DATA_BACKUP_TABLE = """
INSERT <backup_table_name> SELECT * FROM reports;
"""


class Reports:
    def __init__(self):
        pass

    @classmethod
    def _next_id(cls, query):
        return _next_id("schema_dvstream", query)

    @classmethod
    def _table_id(cls, customer_name):
        db_util = DBUtil(schema="schema_dvstream")

        table_id_query = _fill_query_values(REPORTS_SELECT_TABLE_ID_QUERY,
                                          filters={"start_letter": customer_name[0].lower()},)

        logger.info('MODELS Reports table_id query - ' + table_id_query)
        results = db_util.find_all(table_id_query)

        table_id = None
        for row in results:
            table_id = row.get("RUNTABLEONCLICK")
            break

        if table_id is None:
            return table_id

        return table_id


    @classmethod
    def is_name_exists(cls, customer_name):
        db_util = DBUtil(schema="schema_dvstream")

        exists_query = _fill_query_values(REPORTS_SELECT_NAME_EXISTS_QUERY,
                                          filters={"customer_name": str(customer_name).lower()},)

        list = db_util.find_all(exists_query)

        if len(list) == 0:
            return False # not exists
        else:
            return True # exists

    @classmethod
    def _sort_rows(cls, customer_name):
        db_util = DBUtil(schema="schema_dvstream")

        reports_select_name_asc_query = _fill_query_values(REPORTS_SELECT_NAME_ASC_QUERY,
                                                           filters={'start_letter': customer_name[0].lower()},
                                                           surround_quotes=False)

        # get rows in ascending name from table, between 1001 - 1097
        name_asc_query = _fill_query_values(reports_select_name_asc_query)

        logger.info('MODELS Repotrs DEF _sort_rows: name ascending query - ' + name_asc_query)
        list = db_util.find_all(name_asc_query)

        # update row_id and report_id with new values,
        # for report_id keep a higher temp number as it is the primary key to avoid collisions
        temp_report_id = 30001
        new_row_id = 1
        for row in list:
            update_temp_id_query = _fill_query_values(REPORTS_UPDATE_NAME_ASC_TEMP_REPORT_ID_QUERY,
                                              filters={
                                                  "temp_report_id": temp_report_id,
                                                  "new_row_id": new_row_id,
                                                  "report_id": row.get("REPORTID"),
                                                  "row_id": row.get("ROW_ID")
                                              },
                                              surround_quotes=False)
            temp_report_id = temp_report_id + 1
            new_row_id = new_row_id + 1

            logger.info('DEF _sort_rows(cls): update reports with temp report id query - ' + update_temp_id_query)
            db_util.execute(update_temp_id_query)

        # now, set the report_id to regular range between 1001-1097
        table_id = str(cls._table_id(customer_name)) + '01'
        new_report_id = int(table_id)
        temp_report_id = 30001
        for report_id in range(temp_report_id, temp_report_id + len(list)):
            update_report_id_query = _fill_query_values(REPORTS_UPDATE_NAME_ASC_REPORT_ID_QUERY,
                                                        filters={
                                                            "report_id": report_id,
                                                            "new_report_id": new_report_id
                                                        })
            new_report_id = new_report_id + 1
            logger.info('DEF _sort_rows(cls): update reports with new report id query - ' + update_report_id_query)
            db_util.execute(update_report_id_query)



    @classmethod # Create a new Reports row
    def create(cls, customer_name):
        db_util = DBUtil(schema="schema_dvstream")

        if not cls.is_name_exists(customer_name):
            batch_subqry = _fill_query_values(REPORTS_INSERT_BATCH_SUBQUERY,
                                              filters={"customer_name": customer_name}) #Get SQLSCRIPT

            customer_subqry = _fill_query_values(REPORTS_INSERT_CUSTOMER_SUBQUERY,
                                                 filters={"customer_name": customer_name}) # Get ON_CLICK_PASS_SQL

            # report id
            max_report_id_query = _fill_query_values(REPORTS_SELECT_MAX_REPORT_ID_QUERY,
                                                     filters={"start_letter": customer_name[0].lower()},
                                                     surround_quotes=False)
            next_report_id = cls._next_id(max_report_id_query)

            # row id
            max_row_id_query = _fill_query_values(REPORTS_SELECT_MAX_ROW_ID_QUERY,
                                                  filters={"start_letter": customer_name[0].lower()},
                                                  surround_quotes=False)
            next_row_id = cls._next_id(max_row_id_query)

            table_id = cls._table_id(customer_name)

            if next_report_id is None:
                next_report_id = str(table_id) + '01'

            insert_query = _fill_query_values(REPORTS_INSERT_QUERY,
                                              filters={
                                                  "report_id": next_report_id,
                                                  "row_id": next_row_id,
                                                  "table_id": table_id,
                                                  "customer_name": customer_name,
                                                  "batch_subquery": batch_subqry,
                                                  "customer_subquery": customer_subqry
                                              })

            logger.info("DEF create(cls, customer_name): insert sql for reports - " + insert_query)
            db_util.execute(insert_query)

            cls._sort_rows(customer_name)

    @classmethod
    def backup_table(cls):
        # create backup
        db_util = DBUtil(schema="schema_dvstream")

        backup_table_name = "reports_" + datetime.datetime.now().strftime("cs%Y%m%d_%H%M%S")

        create_table_query = _fill_query_values(REPORTS_CREATE_BACKUP_TABLE,
                                                filters={"backup_table_name": backup_table_name},
                                                surround_quotes=False)
        logger.info('DEF backup_table(cls): create backup table query - ' + create_table_query)
        db_util.execute(create_table_query)

        insert_backup_data = _fill_query_values(REPORTS_COPY_DATA_BACKUP_TABLE,
                                                filters={"backup_table_name": backup_table_name},
                                                surround_quotes=False)

        logger.info('DEF backup_table(cls): copying data to backup table query - ' + insert_backup_data)
        db_util.execute(insert_backup_data)


REPORTS_ROWS_INSERT_QUERY = """
INSERT INTO reportsrows
(SORT_ID, TABLE_ID, ROW_ID, TITLE, TITLE_ROW_EXTRA)
VALUES
(<sort_id>, <table_id>, <row_id>, <customer_name>, <style_subquery>)
"""

REPORTS_ROWS_INSERT_STYLE_SUBQUERY = "style='white-space:nowrap;background-color:azure;font-family:arial;font-size:9pt;color:black;text-align:center;height:30'"


REPORTS_ROWS_SELECT_MAX_ROW_ID_QUERY = """
SELECT MAX(ROW_ID) AS MAXID
FROM reportsrows
WHERE SORT_ID > 2100
AND TITLE LIKE '<start_letter>%'
"""

REPORTS_ROWS_SELECT_TITLE_ASC_QUERY = """
SELECT * FROM reportsrows
WHERE SORT_ID > 2100
AND TITLE LIKE '<start_letter>%'
ORDER BY TITLE ASC;
"""

REPORTS_ROWS_UPDATE_TITLE_ASC_TEMP_REPORT_ID_QUERY = """
UPDATE reportsrows SET sort_id = <temp_sort_id>, row_id = <new_row_id>
WHERE
    sort_id = <sort_id>
AND row_id = <row_id>
"""

REPORTS_ROWS_UPDATE_TITLE_ASC_REPORT_ID_QUERY = """
UPDATE reportsrows SET sort_id = <new_sort_id>
WHERE
    sort_id = <sort_id>
"""

REPORTS_ROWS_SELECT_NAME_EXISTS_QUERY = """
SELECT * FROM reportsrows WHERE LOWER(title) = <customer_name>
"""

REPORTS_ROWS_UPDATE_BY_TITLE_QUERY = """
UPDATE reportsrows SET title = <new_title>
WHERE
    title = <current_title>
"""

REPORTS_ROWS_CREATE_BACKUP_TABLE = """
CREATE TABLE <backup_table_name> LIKE reportsrows;
"""

REPORTS_ROWS_COPY_DATA_BACKUP_TABLE = """
INSERT <backup_table_name> SELECT * FROM reportsrows;
"""


class ReportRows:
    def __init__(self):
        pass

    @classmethod
    def is_name_exists(cls, customer_name):
        db_util = DBUtil(schema="schema_dvstream")

        exists_query = _fill_query_values(REPORTS_ROWS_SELECT_NAME_EXISTS_QUERY,
                                          filters={"customer_name": str(customer_name).lower()},)

        list = db_util.find_all(exists_query)

        if len(list) == 0:
            return False # not exists
        else:
            return True # exists


    @classmethod
    def _next_id(cls, query):
        return _next_id("schema_dvstream", query)

    @classmethod
    def _sort_rows(cls, customer_name):
        db_util = DBUtil(schema="schema_dvstream")

        # get rows in ascending title from table, between 1001 - 1097
        title_asc_query = _fill_query_values(REPORTS_ROWS_SELECT_TITLE_ASC_QUERY,
                                             filters={'start_letter': customer_name[0].lower()},
                                             surround_quotes=False)

        logger.info('DEF _sort_rows(cls): title ascending query - ' + title_asc_query)
        list = db_util.find_all(title_asc_query)

        # update row_id and sort_id with new values,
        # for sort_id keep a higher temp number as it is the primary key to avoid collisions
        temp_sort_id = 30001
        new_row_id = 1
        for row in list:
            update_temp_id_query = _fill_query_values(REPORTS_ROWS_UPDATE_TITLE_ASC_TEMP_REPORT_ID_QUERY,
                                              filters={
                                                  "temp_sort_id": temp_sort_id,
                                                  "new_row_id": new_row_id,
                                                  "sort_id": row.get("SORT_ID"),
                                                  "row_id": row.get("ROW_ID")
                                              },
                                              surround_quotes=False)
            temp_sort_id = temp_sort_id + 1
            new_row_id = new_row_id + 1

            logger.info('DEF _sort_rows(cls): update reportsrows with temp sort id query - ' + update_temp_id_query)
            db_util.execute(update_temp_id_query)

        # now, set the report_id to regular range between 1001-1097
        table_id = str(Reports._table_id(customer_name)) + '01'
        new_sort_id = int(table_id)
        temp_sort_id = 30001
        for sort_id in range(temp_sort_id, temp_sort_id + len(list)):
            update_sort_id_query = _fill_query_values(REPORTS_ROWS_UPDATE_TITLE_ASC_REPORT_ID_QUERY,
                                                        filters={
                                                            "sort_id": sort_id,
                                                            "new_sort_id": new_sort_id
                                                        })
            new_sort_id += 1
            logger.info('DEF _sort_rows(cls): update reportsrows with new sort id query - ' + update_sort_id_query)
            db_util.execute(update_sort_id_query)

    @classmethod
    def create(cls, customer_full_name):
        db_util = DBUtil(schema="schema_dvstream")

        if not cls.is_name_exists(customer_full_name):
            style_subquery = _fill_query_values(REPORTS_ROWS_INSERT_STYLE_SUBQUERY)

            table_id = Reports._table_id(customer_full_name)

            report_rows_select_max_row_id_query = _fill_query_values(REPORTS_ROWS_SELECT_MAX_ROW_ID_QUERY,
                                                                     filters={'start_letter': customer_full_name[0].lower()},
                                                                     surround_quotes=False)
            next_row_id = cls._next_id(report_rows_select_max_row_id_query)

            if next_row_id is None:
                next_row_id = 1

            # pad row id with '0', if required
            str_next_row_id = str(next_row_id)
            if int(next_row_id) < 10:
                str_next_row_id = str_next_row_id.zfill(2)

            next_sort_id = str(table_id) + str_next_row_id

            insert_query = _fill_query_values(REPORTS_ROWS_INSERT_QUERY,
                                              filters={
                                                  "sort_id": next_sort_id,
                                                  "row_id": next_row_id,
                                                  "table_id": table_id,
                                                  "customer_name": customer_full_name,
                                                  "style_subquery": style_subquery
                                              })

            logger.info("DEF create(cls, customer_full_name): insert sql for reportsrows - " + insert_query)
            db_util.execute(insert_query)

            cls._sort_rows(customer_full_name)

    @classmethod
    def backup_table(cls):
        # create backup
        db_util = DBUtil(schema="schema_dvstream")

        backup_table_name = "reportsrows_" + datetime.datetime.now().strftime("cs%Y%m%d_%H%M%S")

        create_table_query = _fill_query_values(REPORTS_ROWS_CREATE_BACKUP_TABLE,
                                                filters={"backup_table_name": backup_table_name},
                                                surround_quotes=False)
        logger.info('DEF backup_table(cls): create backup table query - ' + create_table_query)
        db_util.execute(create_table_query)

        insert_backup_data = _fill_query_values(REPORTS_ROWS_COPY_DATA_BACKUP_TABLE,
                                                filters={"backup_table_name": backup_table_name},
                                                surround_quotes=False)

        logger.info('DEF backup_table(cls): copying data to backup table query - ' + insert_backup_data)
        db_util.execute(insert_backup_data)

    @classmethod
    def update_by_title(cls, new_title, current_title):
        db_util = DBUtil(schema="schema_dvstream")

        query = _fill_query_values(REPORTS_ROWS_UPDATE_BY_TITLE_QUERY,
                                   filters={
                                       "current_title": current_title,
                                       "new_title": new_title
                                   })

        logger.info("DEF update_by_title(cls, new_title, current_title): update recordsrows query - " + query)
        db_util.execute(query)


class CustomerSetupModel:
    def __init__(self):
        self.validation_update = None
        self.customer_info = None
        self.search_field_info_list = None
        self.display_names = None
        self.status_message = []

    def set_defaults(self):
        self.validation_update = ValidationUpdate()
        self.customer_info = CustomerInfo()
        self.search_field_info_list = []

        # set default values from property file
        for prop in [self.validation_update, self.customer_info]:
            for k in vars(prop).keys():
                if config.has_option(utils.env, str(k)):
                    value = config.get(utils.env, str(k))
                    prop.__dict__[k] = value.strip()

    def set_name(self, name):
        logger.debug("CustomerSetupModel: set_name - " + name)

        # customer info
        self.customer_info.set_name(name)

        self.validation_update.db_id = config.get(utils.env, "dbid_prefix") + name

        # D:\iFtpSvc\NGC\users\Scholastic\Incoming\
        self.validation_update.new_directory = self.customer_info.ftp_incoming_directory

        # F:\DVOfficeUpdate\Exception\NGC1\
        # No changes as of now, as prefix takes care
        self.validation_update.exception_directory = config.get(utils.env, "exception_directory_prefix")

        # F:\DVOfficeUpdate\Backup\NGC1\Scholastic\
        self.validation_update.backup_directory = config.get(utils.env, "backup_directory_prefix") + name + os.path.sep

        self.validation_update.post_update_query_list = _fill_query_values(POST_UPDATE_QUERY_LIST,
                                                                           filters={"name": name},
                                                                           surround_quotes=False)

    def validate(self):
        for prop in [self.validation_update, self.customer_info]:
            for k in vars(prop).keys():
                # do not have to validate id
                if k in ["id"]:
                    continue

                logger.debug("validating - " + k)
                if prop.__dict__[k] is None:
                        return False

        return True

    @classmethod
    def create(cls, customer_setup):
        ValidationUpdate.backup_table()  # Back up table
        ValidationUpdate.create(customer_setup.validation_update) # Create new
        customer_setup.status_message.append("Created row in VALIDATIONUPDATE Table")

        # add row in dvwebindex settings and create search field info
        # only during adding a new customer
        DVWebIndexFileUpload.backup_table()
        DVWebIndexFileUpload.add_row_dvwebindex_settings(customer_setup.customer_info.name)
        customer_setup.status_message.append("Added row in DBWEBINDEX_SETTINGS Table")

        # create and add search field infos
        #SearchFieldInfo.create(customer_setup.customer_info.name,customer_setup.search_field_info_list)
        customer_setup.status_message.append("Creating SEARCHFIELD_INFO_"+customer_setup.customer_info.name +" Table")
        SearchFieldInfo.create(customer_setup)
        customer_setup.status_message.append("Create completed")

        # insert jobs rows
        Jobs.backup_table()
        Jobs.create(customer_setup.customer_info)
        customer_setup.status_message.append("Added rows to JOBS Table")

        # insert dvstream - reports
        Reports.backup_table()
        Reports.create(customer_setup.customer_info.name)
        customer_setup.status_message.append("Added rows to REPORTS Table")

        # insert dvstream - reportrows
        ReportRows.backup_table()
        ReportRows.create(customer_setup.customer_info.full_name)
        customer_setup.status_message.append("Added rows to REPORTSROWS Table")

        CustomerInfo.write(customer_setup.customer_info)
        customer_setup.status_message.append("Created customer data config file")

        # create directories now
        utils.create_directory(customer_setup.customer_info.ftp_incoming_directory)
        utils.create_directory(customer_setup.customer_info.ftp_outgoing_directory)
        customer_setup.status_message.append("Created SFTP folders")
        customer_setup.status_message.append("- incoming: " + customer_setup.customer_info.ftp_incoming_directory)
        customer_setup.status_message.append("- outgoing: " + customer_setup.customer_info.ftp_outgoing_directory)

    @classmethod
    def update(cls, new_customer_setup, curr_customer_setup=None):
        ValidationUpdate.backup_table()
        ValidationUpdate.update(new_customer_setup.validation_update)
        new_customer_setup.status_message.append("Updated row in VALIDATIONUPDATE Table")

        # user can never change dvwebindex_settings, once created
        # as customer name is added to table only at the time of create

        # for user trying to update a record for which there is no customer_info
        if new_customer_setup.customer_info is not None and \
           new_customer_setup.customer_info.name is not None:
            SearchFieldInfo.backup_table(new_customer_setup.customer_info.name)
            #SearchFieldInfo.update(new_customer_setup.customer_info.name, new_customer_setup.search_field_info_list)
            new_customer_setup.status_message.append("Updating SEARCHFIELD_INFO_"+new_customer_setup.customer_info.name +" Table")
            SearchFieldInfo.update(new_customer_setup)
            new_customer_setup.status_message.append("Update completed")

            Jobs.backup_table()
            Jobs.create(new_customer_setup.customer_info)
            new_customer_setup.status_message.append("Updated rows to JOBS Table")

            if curr_customer_setup and curr_customer_setup.customer_info:
                ReportRows.backup_table()
                ReportRows.update_by_title(new_customer_setup.customer_info.full_name,
                                           curr_customer_setup.customer_info.full_name)
                new_customer_setup.status_message.append("Updated ReportsRows Table")

            CustomerInfo.write(new_customer_setup.customer_info)
            new_customer_setup.status_message.append("Updated customer data config file")
        else:
            logger.info("DEF update(cls, new_customer_setup, curr_customer_setup=None): Customer name not found, ignoring update of search field infos")

    @classmethod
    def clone_from_test(cls, customer_setup):
        test_vu = ValidationUpdate.find_by_dbid(customer_setup.validation_update.db_id, "test")

        if test_vu is not None and \
           len(test_vu) == 1:
            validation_update = test_vu[0]
            customer_setup.validation_update.field_list = validation_update.field_list
            customer_setup.validation_update.task = validation_update.task

            exists = SearchFieldInfo.search_info_table_exists(customer_setup.customer_info.name, "test")
            if exists:
                field_display_map = \
                    SearchFieldInfo.get_display_name_and_field_name_list(customer_setup.customer_info.name, "test")

                display_name_list = cls.display_name_list(customer_setup, field_display_map)
                customer_setup.display_names = "\n".join(display_name_list)

            customer_info = CustomerInfo.load(customer_setup.customer_info.name, "test")
            if customer_info is not None:
                customer_setup.customer_info.full_name = customer_info.full_name
                customer_setup.customer_info.ftp_user_name = customer_info.ftp_user_name
                customer_setup.customer_info.ftp_pwd = customer_info.ftp_pwd

    @classmethod
    def display_name_list(cls, customer_setup, search_fieldinfo_map):
        display_name_list = []
        field_list = customer_setup.validation_update.field_list
        if field_list:
            dvoffFieldList = field_list.split("\n")

            for field in dvoffFieldList:
                field = field.strip()

                # split - FIELD_NAME,TEXT,NO;
                parts = field.split(",")
                if len(parts) > 0:
                    field_name = parts[0]
                    if search_fieldinfo_map.has_key(field_name):
                        display_name_list.append(search_fieldinfo_map.get(field_name))
                    else:
                        display_name_list.append("")

            # Check for required fields in search_fieldinfo_<id>
            requiredFields = ["tracking_number","ship_date","card_number"]
            search_fieldinfo_list = list(search_fieldinfo_map)
            sf_list = [x.lower() for x in search_fieldinfo_list]

            errorMessage = ""
            if "tracking_number" in sf_list and "ship_date" in sf_list and "card_number" in sf_list:
                pass
            else:
                errorMessage = "Table search_fieldinfo_<custonerID> missing required row(s): "
                for f in requiredFields:
                    if f in sf_list:
                        pass
                    else:
                        errorMessage += " '" + f.upper() + '\''
                # TODO Get it to display
                if errorMessage != "":
                    customer_setup.status_message.append("Fix: " + errorMessage)

        return display_name_list

if __name__ == "__main__":
    # Reports.backup_table()
    ReportRows.create("Amoo")
