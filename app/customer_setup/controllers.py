import datetime
import logging

from bottle import route, request, redirect, template, response

from app.server import config
from models import ValidationUpdate, CustomerSetupModel, CustomerInfo, SearchFieldInfo, ResultMessages
import utils

logger = logging.getLogger(__name__)


def set_env(func):
    def inner(**kwargs):
        if kwargs["env"] not in ["test", "production"]:
            raise Exception("Environment not found")

        if kwargs["env"] == "production":
            logger.info("Class set_env(func) - setting db_env - " + kwargs["env"])
            utils.env = kwargs["env"]
        else:
            utils.env = "test"

        response.set_header('Cache-control', 'no-cache, no-store, must-revalidate')
        response.set_header('Pragma', 'no-cache')
        response.set_header('Expires', '0')
        return func(**kwargs)

    return inner


@route("/<env>/customer-setup/start")
@set_env
def new(env):
    return template("customer_setup/start", {"env": env}, debug=True)


@route("/<env>/customer-setup")
@set_env
def customer_list(env):
    try:
        list = ValidationUpdate.find_all()
    except Exception, e:
        print 'Caught an exception'

    data = {
        "list": list,
        "env": env
    }
    return template("customer_setup/list", data, debug=True)


@route("/<env>/customer-setup/search-name", method="POST")
@set_env
def search_name(env):
    name = request.params.name
    if name:
        name = str(name).strip()

    s_name = config.get(utils.env, "dbid_prefix") + name
    is_name_found = _is_customer_exists(name, s_name)

    if is_name_found:
        return template("customer_setup/start", {"db_id": s_name, "env": env}, debug=True)
    else:
        if env:
            redirect("/" + env + "/customer-setup/new?name=" + name)


def _is_customer_exists(name, db_id, env=None):
    if env is None:
        list = ValidationUpdate.find_by_dbid(db_id)
        search_info_exists = SearchFieldInfo.search_info_table_exists(name)
    else:
        list = ValidationUpdate.find_by_dbid(db_id, env)
        search_info_exists = SearchFieldInfo.search_info_table_exists(name, env)

    is_name_found = False
    if len(list) != 0 or search_info_exists:
        is_name_found = True

    return is_name_found


@route("/<env>/customer-setup/new", method="GET")
@set_env
def new(env):
    name = request.query.name

    customer_setup = CustomerSetupModel()
    customer_setup.set_defaults()

    customer_setup.set_name(name)

    is_name_found = False
    clone_test = request.query.cloneTest
    if clone_test:
        CustomerSetupModel.clone_from_test(customer_setup)
        is_name_found = True
    else:
        is_name_found = _is_customer_exists(name, customer_setup.validation_update.db_id, "test")

    customer_setup.customer_info.env = env

    return template("customer_setup/new", {"data": customer_setup, "show_clone": is_name_found}, debug=True)


@route("/<env>/customer-setup/new", method="POST")
@set_env
def add_new(env):
    logger.info("Class add_new(env) - adding new customer setup")
    customer_setup = extract_form(request)
    customer_setup.customer_info.env = env

    options = {}
    if customer_setup.validate():
        try:
            CustomerSetupModel.create(customer_setup) # Do all the work
            logger.info("added successfully")

            options["success"] = "Added new setup successfully"
            options["data"] = customer_setup

            return template("customer_setup/edit", options, debug=True)
        except Exception as e:
            logger.exception("Error adding customer")
            options["error"] = "Error adding customer, Error - " + str(e)

    else:
        options["error"] = "Failed to add new setup, not all required items are provided"

    options["data"] = customer_setup
    return template("customer_setup/new", options, debug=True)


def extract_form(request):
    validation_update = ValidationUpdate()
    customer_info = CustomerInfo()

    # customer info
    customer_info.name = request.forms.get("name")
    customer_info.full_name = request.forms.get("fullName")
    customer_info.date = request.forms.get("date")
    customer_info.ftp_user_name = request.forms.get("ftpUserName")
    customer_info.ftp_pwd = request.forms.get("ftpPassword")
    customer_info.ftp_incoming_directory = request.forms.get("ftpIncomingDirectory")
    customer_info.ftp_outgoing_directory = request.forms.get("ftpOutgoingDirectory")

    # validation update
    validation_update.db_id = request.forms.get("dbId")

    # set default
    validation_update.unique_id = config.get(utils.env, "unique_id")

    validation_update.file_convert_type = request.forms.get("fileConvertType")
    validation_update.field_delimiter = request.forms.get("fieldDelimiter")
    validation_update.new_line_delimiter = request.forms.get("newLineDelimiter")
    validation_update.field_list = request.forms.get("fieldList")
    validation_update.post_update_query_list = request.forms.get("postUpdateQueryList")
    validation_update.new_table = request.forms.get("newTable")
    validation_update.new_directory = request.forms.get("ftpIncomingDirectory")
    validation_update.task = request.forms.get("task")
    validation_update.exception_directory = request.forms.get("exceptionDirectoryHidden")
    validation_update.backup_directory = request.forms.get("backupDirectoryHidden")
    validation_update.email_success_to_list = request.forms.get("emailSuccessToList")
    validation_update.email_error_to_list = request.forms.get("emailErrorToList")
    validation_update.remove_character = request.forms.get("removeCharacter")

    # search field info
    display_name_list = request.forms.get("displayNameList")
    search_field_info_list = SearchFieldInfo.get_info_list_from(validation_update.field_list, display_name_list)

    customer_setup = CustomerSetupModel()
    customer_setup.validation_update = validation_update
    customer_setup.customer_info = customer_info
    customer_setup.search_field_info_list = search_field_info_list
    customer_setup.display_names = display_name_list

    return customer_setup


@route("/<env>/customer-setup/<id>/edit")
@set_env
def edit(env, id):
    validation_update = ValidationUpdate.find_by_id(id)

    customer_setup = CustomerSetupModel()
    customer_setup.validation_update = validation_update

    if validation_update is not None and \
        validation_update.db_id is not None:
        name = validation_update.db_id[len(config.get(utils.env, "dbid_prefix")):]
        customer_setup.customer_info = CustomerInfo.load(name)

        field_and_display_map = SearchFieldInfo.get_display_name_and_field_name_list(name)
        display_name_list = CustomerSetupModel.display_name_list(customer_setup,
                                                                 field_and_display_map)

        if len(display_name_list) > 0:
            customer_setup.display_names = "\n".join(display_name_list)

        if customer_setup.customer_info is None:
            customer_setup.customer_info = CustomerInfo()
            customer_setup.customer_info.set_name(name)

        customer_setup.customer_info.env = env

    return template("customer_setup/edit", data=customer_setup, debug=True)

@route("/<env>/customer-setup/<id>/edit", method="POST")
@set_env
def update(env, id):
    logger.info("Class update(env, id) - updating validation update")

    # get current values
    validation_update = ValidationUpdate.find_by_id(id)

    curr_customer_setup = CustomerSetupModel()
    curr_customer_setup.validation_update = validation_update

    if validation_update is not None and \
       validation_update.db_id is not None:
        name = validation_update.db_id[len(config.get(utils.env, "dbid_prefix")):]
        curr_customer_setup.customer_info = CustomerInfo.load(name)

    # new values from FORM
    new_customer_setup = extract_form(request)
    new_customer_setup.validation_update.id = id
    new_customer_setup.customer_info.env = env

    options = {}
    if new_customer_setup.validate():
        try:
            CustomerSetupModel.update(new_customer_setup, curr_customer_setup)
            logger.info("updated successfully")

            options["success"] = "Updated setup successfully"
            options["data"] = new_customer_setup
        except Exception as e:
            logger.exception("unable to update customer")
            options["error"] = 'Unable to update table search_fieldinfo_' +name+ ', Error - ' + str(e)

    else:
        options["error"] = "Failed to update setup, not all required items are provided"

    options["data"] = new_customer_setup
    return template("customer_setup/edit", options, debug=True)