import os
import ConfigParser
import logging
import bottle
from bottle import run, Bottle, jinja2_template as template, debug, static_file


bottle.TEMPLATE_PATH.append(os.path.abspath(os.path.join(os.getcwd(), ".." + os.path.sep + "views" + os.path.sep)))

bottle.debug(True)

config = ConfigParser.ConfigParser()
config.read(os.path.abspath(os.path.join(os.path.dirname(__file__), ".." + os.path.sep + "config.ini")))


root = logging.getLogger()
root.setLevel(logging.DEBUG)

log_file_path = os.path.abspath(os.path.join(os.getcwd(), ".." + os.path.sep + "logs" + os.path.sep + "customer_setup.log"))

# create console handler
ch = logging.StreamHandler()

# create file handler
fh = logging.FileHandler(filename=log_file_path, mode="w")

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter
fh.setFormatter(formatter)
ch.setFormatter(formatter)

# add ch and fh to logger
root.addHandler(fh)
root.addHandler(ch)

from app.customer_setup.controllers import *


@route("/static/<filepath:path>")
def server_static(filepath):
    return static_file(filepath, root=os.path.abspath(os.path.join(os.path.dirname(__file__), ".." + os.path.sep + "static" + os.path.sep)))

if __name__ == "__main__":
    debug(True)
    run(reloader=True)
