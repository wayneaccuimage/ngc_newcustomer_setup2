$('#btnTextNum').click(
    function() {
        var fieldLists = $('#fieldList').val();

        if (fieldLists) {
            var newLines = [];
            var lines = fieldLists.split('\n');

            lines.forEach(function(element, index) {
                if (element) {
                    var csvs = element.split(',');
                    if (csvs.length && csvs.length == 1) {
                        // no commas, assuming we can add our string
                        var ext = ',TEXT,NO;';
                        newLines.push(csvs + ext);
                    } else {
                        newLines.push(element);
                    }
                }
            });

            if (newLines.length > 0) {
                $('#fieldList').val(newLines.join('\n'));
            }
        }
    }
);

$('#btnLoadDefault').click(
    function() {
        alert("Load Default Field List");
    }
);